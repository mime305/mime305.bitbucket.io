var class_b_n_o055_1_1_b_n_o055 =
[
    [ "__init__", "class_b_n_o055_1_1_b_n_o055.html#adace37d9214a7ca2a1a02cd211f2c0c8", null ],
    [ "change_mode", "class_b_n_o055_1_1_b_n_o055.html#a46879e92a286997c935930164e6d850d", null ],
    [ "get_cal_coeff", "class_b_n_o055_1_1_b_n_o055.html#a0a5a424155e7216d0f052d4d285c7f21", null ],
    [ "get_cal_status", "class_b_n_o055_1_1_b_n_o055.html#a3a2b8d7ed61c3483350ee6d31a823492", null ],
    [ "get_euler", "class_b_n_o055_1_1_b_n_o055.html#a1873696ee7301f1188f5cdbbeb1d1900", null ],
    [ "get_omega", "class_b_n_o055_1_1_b_n_o055.html#a5cc089e83e452a99b718645047c07f43", null ],
    [ "set_cal_coeff", "class_b_n_o055_1_1_b_n_o055.html#a7d537e6a13fda7024fd28ad6c04b9fa2", null ]
];