var searchData=
[
  ['safetyfunction_0',['safetyFunction',['../_s_task_8py.html#a0c4c7b12525514234089754d5fd84bb8',1,'STask']]],
  ['sawwave_1',['SawWave',['../lab01_8py.html#a5ed1c849dde31f8e91790d43db4ba99c',1,'lab01']]],
  ['set_5fcal_5fcoeff_2',['set_cal_coeff',['../class_b_n_o055_1_1_b_n_o055.html#a7d537e6a13fda7024fd28ad6c04b9fa2',1,'BNO055.BNO055.set_cal_coeff()'],['../classtouchpad_1_1_touchpad.html#a2fa4b033ab496139edcde5540eb766e7',1,'touchpad.Touchpad.set_cal_coeff()']]],
  ['set_5fduty_3',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor::Motor']]],
  ['set_5fgain_4',['set_Gain',['../classclosedloop_1_1_closed_loop.html#abe1033e23876424d409cf7ad8118ca24',1,'closedloop::ClosedLoop']]],
  ['set_5freference_5',['set_Reference',['../classclosedloop_1_1_closed_loop.html#ae280c2736be4764a84235da504431ca3',1,'closedloop::ClosedLoop']]],
  ['sinewave_6',['SineWave',['../lab01_8py.html#a8564387711fb2284ea1a5700f63a9f32',1,'lab01']]],
  ['squarewave_7',['SquareWave',['../lab01_8py.html#a76d741c4bb67be71171ae7c7683bc3a7',1,'lab01']]]
];
