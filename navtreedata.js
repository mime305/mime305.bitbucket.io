/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305", "index.html", [
    [ "Links to Assignments and Labs", "index.html#sec_links", null ],
    [ "HW_02", "_h_w02_page.html", null ],
    [ "HW_03", "_h_w03_page.html", null ],
    [ "LAB_01", "_l_a_b01_page.html", [
      [ "Link for Lab 01", "_l_a_b01_page.html#section1", null ],
      [ "Lab 01 State Diagrams", "_l_a_b01_page.html#section2", null ]
    ] ],
    [ "LAB_02", "_l_a_b02_page.html", [
      [ "Links for Lab 02", "_l_a_b02_page.html#section3", null ],
      [ "Lab 02 State Diagrams", "_l_a_b02_page.html#section4", null ],
      [ "Lab 02 Task Diagrams", "_l_a_b02_page.html#section5", null ],
      [ "Lab 02 Data Plot", "_l_a_b02_page.html#section6", null ]
    ] ],
    [ "LAB_03", "_l_a_b03_page.html", [
      [ "Links for Lab 03", "_l_a_b03_page.html#section7", null ],
      [ "Lab 03 State Diagrams", "_l_a_b03_page.html#section8", null ],
      [ "Lab 03 Task Diagrams", "_l_a_b03_page.html#section9", null ],
      [ "Lab 03 Data Plots", "_l_a_b03_page.html#section10", null ]
    ] ],
    [ "LAB_04", "_l_a_b04_page.html", [
      [ "Links for Lab 04", "_l_a_b04_page.html#section11", null ],
      [ "Lab 04 State Diagrams", "_l_a_b04_page.html#section12", null ],
      [ "Lab 04 Task Diagrams", "_l_a_b04_page.html#section13", null ],
      [ "Lab 04 Data Plot", "_l_a_b04_page.html#section14", null ]
    ] ],
    [ "LAB_05", "_l_a_b05_page.html", [
      [ "Links for Lab 05", "_l_a_b05_page.html#section15", null ]
    ] ],
    [ "Term Project", "_t_e_r_m_page.html", [
      [ "Links for Term Project", "_t_e_r_m_page.html#section16", null ],
      [ "Term Project State Diagrams", "_t_e_r_m_page.html#section17", null ],
      [ "Term Project Task Diagrams", "_t_e_r_m_page.html#section18", null ],
      [ "Term Project Data Plot", "_t_e_r_m_page.html#section19", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_b_n_o055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';