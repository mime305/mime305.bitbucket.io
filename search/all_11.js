var searchData=
[
  ['s0_5finit_0',['S0_INIT',['../_e_task_8py.html#a2cb100135fc5342eb90a55c9fedf9504',1,'ETask.S0_INIT()'],['../_i_m_u_task_8py.html#abf910e534c2c10c2443513731dea2c0a',1,'IMUTask.S0_INIT()'],['../lab2___e_task_8py.html#a762a52252dc84d53cc40b6f2543941d0',1,'lab2_ETask.S0_INIT()'],['../lab2___u_task_8py.html#a4c99b96284fbf875589e72d62552e0fc',1,'lab2_UTask.S0_INIT()'],['../_m_task_8py.html#a621ca386bd7d94f588b0fe811264bea3',1,'MTask.S0_INIT()'],['../_s_task_8py.html#afbcd282c6cef5dd97ebff913ee13a0cc',1,'STask.S0_INIT()'],['../_t_task_8py.html#a3868d3a5d620c249d5c6b3b47791767b',1,'TTask.S0_INIT()']]],
  ['s1_5fcmd_1',['S1_CMD',['../lab2___u_task_8py.html#a6ccaa97415f1bb636a36cf7a611448c1',1,'lab2_UTask']]],
  ['s1_5frun_2',['S1_RUN',['../_i_m_u_task_8py.html#a606b47d0b06f92ee82a5e66719c62927',1,'IMUTask.S1_RUN()'],['../_t_task_8py.html#a5d16a70b531d1c09587bb9ac37462977',1,'TTask.S1_RUN()']]],
  ['s1_5fupdate_3',['S1_UPDATE',['../_e_task_8py.html#a0dc288581a76113f5f67b9e62be758ef',1,'ETask.S1_UPDATE()'],['../lab2___e_task_8py.html#a120d0c265e4259e6038927c7b7b7ba81',1,'lab2_ETask.S1_UPDATE()']]],
  ['s1_5fwait_4',['S1_WAIT',['../_m_task_8py.html#a1680015a8b661e881c72c80117907565',1,'MTask.S1_WAIT()'],['../_s_task_8py.html#a919a252ce4ab7a311f366ec6491d977c',1,'STask.S1_WAIT()']]],
  ['s2_5fcalib_5',['S2_CALIB',['../_t_task_8py.html#a281d5dacf03cf32281a061a3d14a6ece',1,'TTask']]],
  ['s2_5fzero_6',['S2_ZERO',['../_e_task_8py.html#a2f793dd33f4f525c9af30dae9786494b',1,'ETask.S2_ZERO()'],['../lab2___e_task_8py.html#a7cfb67cc92140378bc08ba963d268a31',1,'lab2_ETask.S2_ZERO()'],['../lab2___u_task_8py.html#a0fcf62af514a0132fd35291066893f78',1,'lab2_UTask.S2_ZERO()']]],
  ['s3_5fcollect_7',['S3_COLLECT',['../lab2___u_task_8py.html#a95388696592c789a32767c9333abcc8a',1,'lab2_UTask']]],
  ['s3_5fsave_5fcal_5fcoeffs_8',['S3_SAVE_CAL_COEFFS',['../_t_task_8py.html#ae340d67482c43a772570e0e1063c01eb',1,'TTask']]],
  ['s4_5fprint_5fdata_9',['S4_PRINT_DATA',['../lab2___u_task_8py.html#ae7b5ee84995ac01e170c2b9d9af59d5f',1,'lab2_UTask']]],
  ['s4_5fwrite_5fcal_5fcoeffs_10',['S4_WRITE_CAL_COEFFS',['../_t_task_8py.html#aaac93be2a35df1a304b43c75733a446d',1,'TTask']]],
  ['safetyfunction_11',['safetyFunction',['../_s_task_8py.html#a0c4c7b12525514234089754d5fd84bb8',1,'STask']]],
  ['sawwave_12',['SawWave',['../lab01_8py.html#a5ed1c849dde31f8e91790d43db4ba99c',1,'lab01']]],
  ['set_5fcal_5fcoeff_13',['set_cal_coeff',['../class_b_n_o055_1_1_b_n_o055.html#a7d537e6a13fda7024fd28ad6c04b9fa2',1,'BNO055.BNO055.set_cal_coeff()'],['../classtouchpad_1_1_touchpad.html#a2fa4b033ab496139edcde5540eb766e7',1,'touchpad.Touchpad.set_cal_coeff()']]],
  ['set_5fduty_14',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor::Motor']]],
  ['set_5fgain_15',['set_Gain',['../classclosedloop_1_1_closed_loop.html#abe1033e23876424d409cf7ad8118ca24',1,'closedloop::ClosedLoop']]],
  ['set_5freference_16',['set_Reference',['../classclosedloop_1_1_closed_loop.html#ae280c2736be4764a84235da504431ca3',1,'closedloop::ClosedLoop']]],
  ['share_17',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_18',['shares.py',['../shares_8py.html',1,'']]],
  ['sinewave_19',['SineWave',['../lab01_8py.html#a8564387711fb2284ea1a5700f63a9f32',1,'lab01']]],
  ['squarewave_20',['SquareWave',['../lab01_8py.html#a76d741c4bb67be71171ae7c7683bc3a7',1,'lab01']]],
  ['start_5ftime_21',['start_time',['../lab01_8py.html#a2e637aafc10f37f2ab75699c4119916d',1,'lab01']]],
  ['stask_2epy_22',['STask.py',['../_s_task_8py.html',1,'']]],
  ['state_23',['state',['../lab01_8py.html#af4d45ecb7ca77ba06386ba6a21f1fca3',1,'lab01']]]
];
