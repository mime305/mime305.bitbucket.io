var classclosedloop_1_1_closed_loop =
[
    [ "__init__", "classclosedloop_1_1_closed_loop.html#a153c5137699441a7605b9608907daf84", null ],
    [ "get_Actuation", "classclosedloop_1_1_closed_loop.html#ad24ed6b0ba7a0b04e45d9265e99cde80", null ],
    [ "get_Kp", "classclosedloop_1_1_closed_loop.html#aabfd126eb373a40747f7fd312ed0056c", null ],
    [ "get_Reference", "classclosedloop_1_1_closed_loop.html#a1e8661da077f6829bb7e784a3a52eb71", null ],
    [ "run", "classclosedloop_1_1_closed_loop.html#a40c9d1cc7bf944fd0cf5ac48ac4f081d", null ],
    [ "set_Gain", "classclosedloop_1_1_closed_loop.html#abe1033e23876424d409cf7ad8118ca24", null ],
    [ "set_Reference", "classclosedloop_1_1_closed_loop.html#ae280c2736be4764a84235da504431ca3", null ]
];