"""!

@page LAB02_page LAB_02
@details        This Lab was designed to perform different blinking patterns of 
                the LED on the nucleo. 

@section section1 Link to Lab 02
           Here are the links to all the complete file and details for Lab02.
           File: @ref .py
           File: @ref .py
           File: @ref .py

@section section2 Lab 01 State Diagrams
     @image html LAB01ST1.png width=500px
     @image html LAB01ST2.png width=500px
     
"""

