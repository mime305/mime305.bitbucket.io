var _t_task_8py =
[
    [ "calInfo", "_t_task_8py.html#af8ab8f62d158544bdf7f6ce4d7ab8fdc", null ],
    [ "getReady", "_t_task_8py.html#ab9f03924b94543575a25b5d17262a0f2", null ],
    [ "TouchpadFunction", "_t_task_8py.html#a295f91f155ec4104e5f38b7fa5c132f5", null ],
    [ "CAL_BOTTOM_LEFT", "_t_task_8py.html#aa05e5cf9f8aca9a626d84a09cbc70d03", null ],
    [ "CAL_BOTTOM_RIGHT", "_t_task_8py.html#ad8981d37d05248e8b58a0694f525f6d4", null ],
    [ "CAL_CENTER", "_t_task_8py.html#ab58a3d5bca15209b5fd725e8c0cf12ba", null ],
    [ "CAL_TOP_LEFT", "_t_task_8py.html#a459412226052a1c6d034811a3adad814", null ],
    [ "CAL_TOP_RIGHT", "_t_task_8py.html#a18366a450f6b788b9823f8b08ddc8905", null ],
    [ "S0_INIT", "_t_task_8py.html#a3868d3a5d620c249d5c6b3b47791767b", null ],
    [ "S1_RUN", "_t_task_8py.html#a5d16a70b531d1c09587bb9ac37462977", null ],
    [ "S2_CALIB", "_t_task_8py.html#a281d5dacf03cf32281a061a3d14a6ece", null ],
    [ "S3_SAVE_CAL_COEFFS", "_t_task_8py.html#ae340d67482c43a772570e0e1063c01eb", null ],
    [ "S4_WRITE_CAL_COEFFS", "_t_task_8py.html#aaac93be2a35df1a304b43c75733a446d", null ],
    [ "xC", "_t_task_8py.html#a53552309071867f72a8e39edb08e602e", null ]
];