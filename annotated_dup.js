var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "class_b_n_o055_1_1_b_n_o055.html", "class_b_n_o055_1_1_b_n_o055" ]
    ] ],
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1_closed_loop.html", "classclosedloop_1_1_closed_loop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "class_d_r_v8847_1_1_d_r_v8847.html", "class_d_r_v8847_1_1_d_r_v8847" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1_encoder.html", "classencoder_1_1_encoder" ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1_motor.html", "classmotor_1_1_motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1_queue.html", "classshares_1_1_queue" ],
      [ "Share", "classshares_1_1_share.html", "classshares_1_1_share" ]
    ] ],
    [ "touchpad", null, [
      [ "Touchpad", "classtouchpad_1_1_touchpad.html", "classtouchpad_1_1_touchpad" ]
    ] ]
];