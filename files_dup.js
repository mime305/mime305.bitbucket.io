var files_dup =
[
    [ "BNO055.py", "_b_n_o055_8py.html", [
      [ "BNO055.BNO055", "class_b_n_o055_1_1_b_n_o055.html", "class_b_n_o055_1_1_b_n_o055" ]
    ] ],
    [ "DRV8847.py", "_d_r_v8847_8py.html", [
      [ "DRV8847.DRV8847", "class_d_r_v8847_1_1_d_r_v8847.html", "class_d_r_v8847_1_1_d_r_v8847" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1_encoder.html", "classencoder_1_1_encoder" ]
    ] ],
    [ "ETask.py", "_e_task_8py.html", "_e_task_8py" ],
    [ "HW0x02.py", "_h_w0x02_8py.html", null ],
    [ "HW0x03.py", "_h_w0x03_8py.html", null ],
    [ "IMUTask.py", "_i_m_u_task_8py.html", "_i_m_u_task_8py" ],
    [ "lab01.py", "lab01_8py.html", "lab01_8py" ],
    [ "lab2_ETask.py", "lab2___e_task_8py.html", "lab2___e_task_8py" ],
    [ "lab2_UTask.py", "lab2___u_task_8py.html", "lab2___u_task_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.Motor", "classmotor_1_1_motor.html", "classmotor_1_1_motor" ]
    ] ],
    [ "MTask.py", "_m_task_8py.html", "_m_task_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1_share.html", "classshares_1_1_share" ],
      [ "shares.Queue", "classshares_1_1_queue.html", "classshares_1_1_queue" ]
    ] ],
    [ "STask.py", "_s_task_8py.html", "_s_task_8py" ],
    [ "touchpad.py", "touchpad_8py.html", "touchpad_8py" ],
    [ "TTask.py", "_t_task_8py.html", "_t_task_8py" ]
];